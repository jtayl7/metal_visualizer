from band_scraper import band_scrape
from lyrics_scraper import lyrics_scrape
from genre_cloud_generator import generate_cloud

band_scrape('band_list.xlsx', 'demo', 1)
lyrics_scrape('band_scrape.json')
generate_cloud('lyric_scrape.json', 100)