import pandas as pd
import requests
from bs4 import BeautifulSoup
from time import sleep
from random import randint
import json

def band_scrape (band_spreadsheet = '.\\band_list.xlsx', sheet = 'bands', no_songs = 10):
    df_band_names = pd.ExcelFile(band_spreadsheet).parse(sheet)   
    bands = []
    for index, row in df_band_names.iterrows():
        band_name = row['band']
        print(f'Fetching {band_name} top {no_songs} songs...')
        band_name_formatted = row['band'].replace(" ", "-").rstrip()
        genre = row['genre']
        band_url = f"http://www.songlyrics.com/{band_name_formatted}-lyrics/"    
        response = requests.get(band_url, timeout=5)
        soup = BeautifulSoup(response.content, "html.parser")
        if soup.findAll("div", {"class": "search-results"}):   #Skip bands that are not found
            print(f'----Band not found: {band_name}')
            continue
        song_url = soup.find("tbody").find_all("a", itemprop="url")

        #fetch links to the top songs of each band
        for i in range (no_songs):
            bands.append({
                "genre" : genre,
                "band" : band_name,
                "title" : song_url[i].text,
                "lyrics_url" : song_url[i]["href"],
                "lyrics" : "",
                "tokens" : []
            })
        sleep(randint(2, 7))   #scrape responsibly

    with open("band_scrape.json", "w") as write_file:
        json.dump(bands, write_file)