import json
from bs4 import BeautifulSoup, SoupStrainer
from nltk.tokenize import TweetTokenizer
import requests
from time import sleep
from random import randint

def lyrics_scrape(band_scrape = '.\\band_scrape.json'):
    with open(band_scrape, "r") as read_file:
        bands = json.load(read_file)

    soupStrainer = SoupStrainer(id="songLyricsDiv") 
    save_regularly = 99   
    for song in bands:
        print(f"Scraping {song['title']} lyrics...", end=" ")
        lyrics_page = requests.get(song['lyrics_url'], timeout=5)
        if lyrics_page.status_code is 200:
            soup = BeautifulSoup(lyrics_page.content, "html.parser", parse_only=soupStrainer)
            song['lyrics'] = soup.text
            song['tokens'] = TweetTokenizer().tokenize(soup.text)
        else:
            print(f"####Error####: {song['title']} lyrics not found")
        if save_regularly == 99:
            with open("lyric_scrape.json", "w") as write_file:
                json.dump(bands, write_file)
            save_regularly = 0
        else:
            save_regularly += 1
        print('complete.')
        sleep(randint(2, 10))   #scrape responsibly

    with open("lyric_scrape.json", "w") as write_file:
        json.dump(bands, write_file)