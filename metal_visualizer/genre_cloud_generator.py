import json
import nltk
from nltk.corpus import stopwords
from wordcloud import WordCloud, ImageColorGenerator
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np

def generate_cloud(lyric_scrape = '.\\lyric_scrape.json', cloud_word_size = 100):
    with open(lyric_scrape) as f:
        data = json.load(f)
    
    #resource paths
    font = '..\\resources\\squealer.ttf'
    color = '..\\resources\\fire_hd.jpg'
    mask = '..\\resources\\metal_mask.jpg'

    #stop words
    nltk_stopwords = set(nltk.corpus.stopwords.words('english'))
    punctutation_stop = [
        '?', ',', '’', '.', '\'', '\"', '[', ']', '!', '...', '(', ')',
        '*', '=', '€', '¸'
        ]
    custom_stop = [
        'what\'s', 'there\'s', 'that\'s', 'oh', 'ooh', 'yeah', 'hey', 'get',
        'got', '™', 'TM', 'ain\'t', 'x2', 'x3', 'x4', 'x5', 'gonna', "i've",
        "i'm", 'gonna', "i've", "i'm", "i'll", 'come', 'coming' 'we', 'us',
        'let', 'know', 'go', 'much', 'let', 'ever', '-', 'back', 'takes', 'og',
        'en', 'cause', 'whoa', 'ah', 'tha', 'like', 'wanna'
        ]
    default_stopwords = list(nltk_stopwords) + punctutation_stop
    vertical_stopwords = default_stopwords + custom_stop

    #aggregate tokens for each genre
    genres = {
        'Groove' : [],
        'Metalcore' : [],
        'Thrash' : [],
        'Black' : [],
        'Glam' : [],
        'Heavy' : [],
        'Nu' : [],
        'Industrial' : [],
        'Grunge' : [],
        'Progressive' : [],
        'Power' : []
        }

    print('Sorting tokens into genres...')
    for index in range(len(data)):
        if 'We do not have the lyrics for' in data[index]['lyrics']:
            #DEBUG
            # print('----Song skipped, no lyrics found: ' +
                #data[index]['genre'] + ' - ' + data[index]['band'] +
                #' - ' + data[index]['title'])
            continue
        elif data[index]['genre'] == 'Groove':
            genres['Groove'].extend(data[index]['tokens'])
        elif data[index]['genre'] == 'Metalcore':
            genres['Metalcore'].extend(data[index]['tokens'])
        elif data[index]['genre'] == 'Thrash':
            genres['Thrash'].extend(data[index]['tokens'])
        elif data[index]['genre'] == 'Black':
            genres['Black'].extend(data[index]['tokens'])
        elif data[index]['genre'] == 'Glam':
            genres['Glam'].extend(data[index]['tokens'])
        elif data[index]['genre'] == 'Heavy':
            genres['Heavy'].extend(data[index]['tokens'])
        elif data[index]['genre'] == 'Nu':
            genres['Nu'].extend(data[index]['tokens'])
        elif data[index]['genre'] == 'Industrial':
            genres['Industrial'].extend(data[index]['tokens'])
        elif data[index]['genre'] == 'Grunge':
            genres['Grunge'].extend(data[index]['tokens'])
        elif data[index]['genre'] == 'Progressive':
            genres['Progressive'].extend(data[index]['tokens'])
        elif data[index]['genre'] == 'Power':
            genres['Power'].extend(data[index]['tokens'])   

    #generate word clouds
    print('Generating word clouds...')
    metal_mask = np.array(Image.open(mask))
    metal_color = ImageColorGenerator(np.array(Image.open(color)))
    for lyric_tokens in genres:
        if (len(genres[lyric_tokens]) > 0):
            lower_case_tokens = [token.lower() for token in genres[lyric_tokens]]
            default_frequency = nltk.FreqDist(
                [token for token in lower_case_tokens if token not in default_stopwords])
            vertical_frequency = nltk.FreqDist(
                [token for token in lower_case_tokens if token not in vertical_stopwords])

            #DEBUG
            # if(True):
            #     print(lyric_tokens)
            #     for word, frequency in vertical_frequency.most_common(70):
            #         print(word + " : " + str(frequency))
            #         ('\n')
            
            default_wordcloud = WordCloud(
                font_path=font, width = 1077, height = 1100, max_words=cloud_word_size, 
                mask=metal_mask, contour_width=7, contour_color='firebrick', prefer_horizontal=1, 
                relative_scaling=.8).generate_from_frequencies(default_frequency)
            vertical_wordcloud = WordCloud(
                font_path=font, width = 1077, height = 1100, max_words=cloud_word_size, 
                mask=metal_mask, contour_width=7, contour_color='firebrick', prefer_horizontal=1, 
                relative_scaling=.8).generate_from_frequencies(vertical_frequency)            
            
            plt.axis("off")
            plt.title(lyric_tokens)
            plt.imshow(default_wordcloud.recolor(color_func=metal_color), interpolation="bilinear")
            plt.imshow(vertical_wordcloud.recolor(color_func=metal_color), interpolation="bilinear")
            default_wordcloud.to_file(f'..\\output\\{lyric_tokens}_default.jpg')
            vertical_wordcloud.to_file(f'..\\output\\{lyric_tokens}_vertical.jpg')
            print(f'{lyric_tokens} clouds completed...')
        else:
            print(f"No lyrics for the {lyric_tokens} genre.")