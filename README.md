# Metal Lyrics Visualizer

The Metal Lyrics Visualizer (MLV) is a Python application that generates
custom word clouds for different sub-genres of metal music. There are two word
clouds generated for each genre submitted. One word cloud contains the lyrics of the
genre minus standard stopwords and punctuation and on top of that the other word cloud
incoporates an additional 50+ stopwords tailored specifically to enhance metal lyric
based word clouds. It takes an Excel spreadsheet containing a list of bands and their
genres and fetches lyrics of artist's most popular songs on Songlyrics.com. It
then aggregates the lyrics and generates word clouds for each genre.  

Currently the Metal Lyrics Analyzer supports the following genres:
* Heavy
* Black
* Nu
* Progressive
* Grunge (I know, controversial)
* Glam
* Power
* Thrash
* Groove
* Industrial
* Metalcore


## Getting Started

These instructions will get a copy of the project up and running on your local machine for evaluation purposes. Non-windows users may need to make minor adjustments to accomodate their specific operating system.

### Prerequisites

Metal Lyrics Visualizer requires Python 3 with the following packages installed:

```
pip install pandas
pip install beautifulsoup4
pip install requests
pip install nltk
pip install matplotlib
pip install wordcloud
pip install xlrd

```
One you have NLTK installed you will need to download the English (default) standard stopwords. The easiest way to do this is to open a Python terminal and enter the following command:

```
>>> import nltk
>>> nltk.download('stopwords')
>>> exit()
```


### Installing

Once you have the prerequisites installed, download the Metal Lyrics Visualizer from its Git repo (wherever you chose to download the repo will be referred to as the MLV home folder from this point forward).

```
git clone https://gitlab.com/jtayl7/metal_visualizer.git
```

### Demo Mode

MLV comes preconfigured for demo mode, in this mode it is limited to 1 song each from 11 bands. As you can imagine the resulting
word clouds are not terribly interesting but it allows you to quickly see the tool in action. The generated word clouds, 11 default and 11 vertical, will be found in the '.\output' folder.

To run MLV in demo mode
* Open your terminal and navigate to the MLV home folder
* Switch to the '\metal_visualizer' folder
* Run 'metal_vizualizer.py' 

```
python .\metal_visualizer.py 
```

### Cached Mode

In this mode all songs from the included band list have already been downloaded and processed so you can generate new word
clouds without having to scrape 1100 songs yourself. The generated word clouds, 11 default and 11 vertical, will be found in the '.\output' folder.

To run MLV in cached mode
* Open your terminal and navigate to the MLV home folder
* Select and copy 'band_scrape.json' and 'lyric_scrape.json' from the '\samples' folder to the '\metal_visualizer' folder
* Switch to the '\metal_visualizer' folder
* Edit and save 'metal_vizualizer.py' with the following change (comment out band_scrape and lyrics_scrape functions)

```
#band_scrape('band_list.xlsx', 'demo', 1)
#lyrics_scrape('band_scrape.json')
generate_cloud('lyric_scrape.json', 100)
```

* Run 'metal_vizualizer.py.'
  

### Full Mode

In this mode MLV will start completely from scratch, it will first fetch information on the 10 most popular songs from 100 different artists. It will then scrape the lyrics of 1100 songs, process them and finally generates the word clouds. Please note doing so will overwrite all existing json files and initiate scraping from scratch. To err on the safe side and scrape responsibly, MLV will iniate requests at a random rate of 2 to 10 seconds per request. As a result running in this mode can take 3-4 hours to complete.
The generated word clouds, 11 default and 11 vertical, will be found in the '.\output' folder.

To run MLV full mode
* Open your terminal and navigate to the MLV home folder
* Switch to the '\metal_visualizer' folder
* Delete any existing 'band_scrape.json' and 'lyric_scrape.json' files
* Edit and save 'metal_vizualizer.py' with the following change (demo to bands, 1 to 10)

```
band_scrape('band_list.xlsx', 'bands', 10)
lyrics_scrape('band_scrape.json')
generate_cloud('lyric_scrape.json', 100)
```
* Run 'metal_vizualizer.py'

```
python .\metal_visualizer.py 
```

### Cloud Preview
If you'd like to see the end results without having to run anything, you can navigate to the 'samples' folder where you'll find word
cloud images that have already been generated from a previous Full Mode run.

## Customizing MLV

### Adding your own bands

* Edit '.\metal_visualizer\band_list.xlsx'
* Switch to the 'band' sheet
* Add the genre and the artist in their respective columns
* Save and rerun MLV


### Increasing the number of songs per band (default = 10)

* Edit '.\metal_visualizer\metal_visualizer.py'
* Add a third parameter (int only) to the band_scraper() function
* Example on increasing to 20 songs
```
band_scrape('band_list.xlsx', 'bands', 20)
```
* Save and rerun MLV

### Setting the number of words to be included in each word cloud (default = 100)

* Edit '.\metal_visualizer\metal_visualizer.py'
* Add a second parameter (int only) to the generate_cloud() function
* Example on decreasing word count to 50
```
generate_cloud('lyric_scrape.json', 50)
```
* Save and rerun MLV

## Author

* **JT**

## Acknowledgments

* metal
* stackoverflow
* cs410
